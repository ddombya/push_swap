/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 11:41:30 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/17 17:56:46 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "get_next_line.h"
# include "libft.h"
# include <ctype.h>
# include <stdio.h>

# define LIST_A 0
# define LIST_B 1
# define BOTH_LIST 2
# define FLAG -1
# define PRINT_OP 3
# define OPT_V 1
# define OPT_S 4
# define OPT_L 8

enum				e_op
{
	op_s = 0,
	op_p = 3,
	op_r = 5,
	op_rr = 8
};

typedef struct		s_ps
{
	t_node			*node;
	t_stack			list[2];
	int				orders[2];
	char			*argv;
	int				std_in;
	int				std_out;
	int				opt;
}					t_ps;

void				push_swap_init(t_ps *elem, int ac, char **av);
void				push_swap_exit(t_ps *elem, int status);

t_node				*stack_head(t_ps *elem, uint8_t stack);
void				view_stack(t_ps *elem, uint8_t stack);
int					ft_is_sort(t_ps *elem, uint8_t stack, int len);

void				swap(t_ps *elem, uint8_t stack);
void				push(t_ps *elem, uint8_t stack);
void				r_rotate(t_ps *elem, uint8_t stack);
void				rotate(t_ps *elem, uint8_t stack);

void				short_sort(t_ps *elem, uint8_t stack, size_t len);
void				sort(t_ps *elem, uint8_t stack, size_t len);
void				quicksort(t_ps *elem, uint8_t stack, size_t len);
void				selectsort(t_ps *elem, uint8_t stack);

#endif
