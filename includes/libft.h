/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 12:37:44 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/09 19:41:38 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <ctype.h>
# include <limits.h>
# define LOWER(x) ((x) >= 'a' && (x) <= 'z')
# define UPPER(x) ((x) >= 'A' && (x) <= 'Z')
# define DIGIT(x) ((x) >= '0' && (x) <= '9')

typedef struct		s_node
{
	struct s_node	*next;
	struct s_node	*prev;
	int				nb;
}					t_node;

typedef struct		s_stack
{
	struct s_node	*head;
	struct s_node	*tail;
	size_t			len;
}					t_stack;

int					ft_atoi(const char *s);
void				ft_bzero(void *s, size_t n);
void				ft_error(void);
int					ft_isdigit(int c);
int					ft_isspace(int c);
void				*ft_memalloc(size_t size);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memset(void *s, int c, size_t n);
void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putnbr(int n);
void				ft_putnbr_fd(int n, int fd);
void				ft_putnstr(char const *s, int len);
void				ft_putstr(char const *s);
void				ft_putstr_fd(const char *s, int fd);
char				*ft_strchr(const char *s, int c);
void				ft_strclr(char *s);
int					ft_strcmp(const char *s1, const char *s2);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strcpy(char *dest, const char *src);
void				ft_strdel(char **as);
char				*ft_strdup(const char *s);
char				*ft_strjoin(char const *s1, char const *s2);
size_t				ft_strlen(const char *s);
char				*ft_strnew(size_t size);
char				*ft_strsub(char const *s, unsigned int start, size_t len);

t_node				*ft_insert_elem(t_node *elem, t_node *prev, t_node *next);
t_node				*ft_push_elem(t_node *elem, t_node *prev, t_node *next);

t_node				*ft_put_on_stack(t_node *elem, t_node *data);
t_node				*ft_pop_on_stack(t_node *elem, t_node *data);

t_node				*ft_stack_push(t_stack *elem, t_node *data);
t_node				*ft_stack_pop(t_stack *elem, t_node *data);
void				init_stack(t_stack *elem);

t_node				*ft_swap_node(t_node *elem);

#endif
