/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack_push.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 14:37:33 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/14 22:06:11 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

t_node		*ft_stack_push(t_stack *elem, t_node *data)
{
	elem->len++;
	return (ft_put_on_stack((t_node *)elem, data));
}

t_node		*ft_stack_pop(t_stack *elem, t_node *data)
{
	elem->len++;
	return (ft_pop_on_stack((t_node *)elem, data));
}

void		init_stack(t_stack *elem)
{
	ft_memset(elem, 0, sizeof(t_stack));
	elem->head = (struct s_node *)elem;
	elem->tail = (struct s_node *)elem;
}
