/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insert_elem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 14:25:55 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/14 22:09:54 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

inline t_node	*ft_insert_elem(t_node *elem, t_node *prev, t_node *next)
{
	elem->next = prev->next;
	elem->prev = next->prev;
	prev->next = elem;
	next->prev = elem;
	return (elem);
}

inline t_node	*ft_push_elem(t_node *elem, t_node *prev, t_node *next)
{
	next->prev = prev;
	prev->next = next;
	elem->prev = elem;
	elem->next = elem;
	return (elem);
}
