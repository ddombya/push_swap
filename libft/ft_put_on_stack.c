/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_on_stack.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 14:32:53 by ddombya           #+#    #+#             */
/*   Updated: 2018/06/26 03:37:24 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

inline t_node	*ft_put_on_stack(t_node *elem, t_node *data)
{
	return (ft_insert_elem(data, elem->prev, elem));
}

t_node			*ft_pop_on_stack(t_node *elem, t_node *data)
{
	return (ft_insert_elem(data, elem, elem->next));
}
