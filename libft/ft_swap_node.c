/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_node.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/23 20:19:21 by ddombya           #+#    #+#             */
/*   Updated: 2018/05/23 20:22:02 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

t_node		*ft_swap_node(t_node *elem)
{
	t_node *tmp;

	tmp = elem->next;
	elem->next = tmp->next;
	tmp->next = elem;
	tmp->prev = elem->prev;
	elem->prev = tmp;
	tmp->prev->next = tmp;
	elem->next->prev = elem;
	return (elem);
}
