/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 10:35:04 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/17 16:47:43 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

inline int		ft_atoi(const char *s)
{
	long	nb;
	int		neg;

	nb = 0;
	neg = 0;
	if (!ft_isdigit(*s) && *s != '-' && *s != '+')
		ft_error();
	else if (!ft_strcmp("-", s))
		ft_error();
	if (*s == '-')
		neg = *s++ > 0;
	else if (*s == '+')
		s++;
	while (*s)
	{
		if (!ft_isdigit(*s) || (nb = nb * 10 + *s++ - '0') >
				(neg ? (long)INT_MAX : INT_MAX))
			ft_error();
	}
	return (neg ? -nb : nb);
}
