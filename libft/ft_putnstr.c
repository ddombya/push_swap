/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:24:41 by ddombya           #+#    #+#             */
/*   Updated: 2018/04/29 17:26:43 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib.h"

void		ft_putnstr(const char *s, int len)
{
	char c;

	if (s)
	{
		while (len--)
		{
			c = *s;
			write(1, &c, 1);
			s++;
		}
	}
	return ;
}
