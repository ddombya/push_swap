/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 17:04:39 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/09 18:50:33 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void		sort_two(t_ps *elem, uint8_t stack, size_t len)
{
	t_node	*node;
	int		fst;
	int		snd;

	if (len != 2)
		return ;
	if (!(node = stack_head(elem, stack)))
		return ;
	fst = node->nb * elem->orders[stack];
	snd = node->next->nb * elem->orders[stack];
	if (fst > snd)
		swap(elem, stack);
}

static void		sort_three(t_ps *elem, uint8_t stack, size_t len)
{
	t_node	*node;
	int		fst;
	int		snd;
	int		trd;

	if (!(node = stack_head(elem, stack)))
		return ;
	fst = node->nb * elem->orders[stack];
	snd = node->next->nb * elem->orders[stack];
	trd = node->next->next->nb * elem->orders[stack];
	if (fst > snd && snd < trd && fst < trd)
		swap(elem, stack);
	else if (fst < snd && trd < fst)
		r_rotate(elem, stack);
	else if (fst > snd && snd < trd)
		rotate(elem, stack);
	else if (fst > snd && snd > trd)
		swap(elem, stack);
	else if (fst > snd && snd < trd)
		r_rotate(elem, stack);
	else if (fst < snd && snd > trd && fst < trd)
		r_rotate(elem, stack);
	else
		return ;
	sort_three(elem, stack, len);
}

void			short_sort(t_ps *elem, uint8_t stack, size_t len)
{
	t_node	*node;
	int		snd;
	int		trd;

	if (len != 3)
		return (sort_two(elem, stack, len));
	if (elem->list[stack].len == 3)
		return (sort_three(elem, stack, len));
	sort_two(elem, stack, 2);
	if (!(node = stack_head(elem, stack)))
		return ;
	snd = node->next->nb * elem->orders[stack];
	trd = node->next->next->nb * elem->orders[stack];
	if (snd > trd)
	{
		rotate(elem, stack);
		swap(elem, stack);
		r_rotate(elem, stack);
	}
	sort_two(elem, stack, 2);
}

void			sort(t_ps *elem, uint8_t stack, size_t len)
{
	if (len <= 3)
		return (short_sort(elem, stack, len));
	if (elem->list[stack].len <= 20)
		return (selectsort(elem, stack));
	else
		return (quicksort(elem, stack, len));
}
