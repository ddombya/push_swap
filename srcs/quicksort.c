/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 14:00:50 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/09 18:46:40 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int		list_forward(t_ps *elem, uint8_t stack, size_t len, int center)
{
	t_node *node;

	if (!(node = stack_head(elem, stack)))
		return (0);
	while (len--)
	{
		if ((node->nb * elem->orders[stack]) < center)
			return (1);
		node = node->next;
	}
	return (0);
}

static int		get_center(t_ps *elem, uint8_t stack, size_t len)
{
	t_node		*node;
	int			min;
	int			max;
	int			center;

	if (!(node = stack_head(elem, stack)))
		return (0);
	min = INT_MAX;
	max = 0;
	while (len-- && node != (t_node *)(elem->list + stack))
	{
		if (node->nb < min)
			min = node->nb;
		if (node->nb > max)
			max = node->nb;
		node = node->next;
	}
	center = (((max - min) / 2) + min) * elem->orders[stack];
	return (center);
}

void			quicksort(t_ps *elem, uint8_t stack, size_t len)
{
	int			center;
	uint8_t		p;
	uint8_t		top_half_len;
	size_t		i;

	p = 0;
	top_half_len = 0;
	i = 0;
	center = get_center(elem, stack, len);
	while (list_forward(elem, stack, len, center) && i++ < len)
		if ((stack_head(elem, stack)->nb * elem->orders[stack]) < center)
			push(elem, (stack + (p++ & 0)));
		else
			rotate(elem, (stack + (top_half_len++ & 0)));
	stack ? sort(elem, (stack ^ 1), p) : 0;
	if (top_half_len > (elem->list[stack].len / 2) && elem->list[stack].len > 3)
		while (top_half_len++ < elem->list[stack].len)
			rotate(elem, stack);
	else if (elem->list[stack].len > 3)
		while (top_half_len--)
			r_rotate(elem, stack);
	sort(elem, stack, len - p);
	!stack ? sort(elem, (stack ^ 1), p) : 0;
	while (p--)
		push(elem, (stack ^ 1));
}
