/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_fcts.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 16:17:55 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/18 18:03:17 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void				arg_stack(char **av, t_ps *elem)
{
	char				*end;
	t_node				*tmp;

	elem->argv = av[1];
	end = elem->argv;
	tmp = elem->node;
	while (*end)
	{
		while ((ft_isdigit(*end) && *end) || *end == '-')
			end++;
		if (!*end)
		{
			tmp->nb = ft_atoi(elem->argv);
			ft_stack_push(elem->list + LIST_A, tmp++);
			break ;
		}
		*end = '\0';
		tmp->nb = ft_atoi(elem->argv);
		ft_stack_push(elem->list + LIST_A, tmp++);
		end++;
		elem->argv = end;
		while (ft_isspace(*end))
			end++;
	}
}

static void				create_stack(t_ps *elem, char **av, int n)
{
	t_node				*tmp;

	init_stack(elem->list + LIST_A);
	init_stack(elem->list + LIST_B);
	if (!(elem->node = malloc(1024 * sizeof(t_node))))
		return ;
	tmp = elem->node;
	if (n == 2)
		arg_stack(av, elem);
	else
	{
		while (*av)
		{
			tmp->nb = ft_atoi(*av++);
			ft_stack_push(elem->list + LIST_A, tmp++);
		}
	}
}

void					push_swap_init(t_ps *elem, int ac, char **av)
{
	ft_memcpy(elem->orders, (int[2]){1, -1}, 2 * sizeof(int));
	if (ac == 1)
		return ;
	if (ac == 2)
		create_stack(elem, av, ac);
	else
		create_stack(elem, av + 1, ac);
}
