/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_fcts.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 18:50:52 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/17 17:56:57 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

inline t_node		*stack_head(t_ps *elem, uint8_t stack)
{
	t_stack *tmp;

	tmp = elem->list + stack;
	return (tmp->len ? (t_node *)tmp->head : NULL);
}

inline void			view_stack(t_ps *elem, uint8_t stack)
{
	t_node	*node;

	if (!(node = stack_head(elem, stack)))
		return ;
	if (stack == BOTH_LIST)
	{
		view_stack(elem, LIST_A);
		return (view_stack(elem, LIST_B));
	}
	stack ? ft_putstr("B -> ") : ft_putstr("A -> ");
	while (node != (t_node *)(elem->list + stack))
	{
		ft_putstr("[");
		ft_putnbr(node->nb);
		ft_putstr("]");
		node = node->next;
		if (node != (t_node *)(elem->list + stack))
			ft_putchar(' ');
	}
	ft_putchar('\n');
}

int					ft_is_sort(t_ps *elem, uint8_t stack, int len)
{
	t_node	*node;
	t_node	*tmp;

	if (len == FLAG)
		len = (int)elem->list[stack].len;
	else if (elem->list[LIST_B].len)
		return (0);
	if (!(node = stack_head(elem, stack)))
		return (1);
	while (len && node != (t_node *)elem->list[stack].tail)
	{
		tmp = node->next;
		if (elem->orders[stack] == 1 && node->nb > tmp->nb)
			return (0);
		if (elem->orders[stack] == -1 && node->nb < tmp->nb)
			return (0);
		node = tmp;
		len--;
	}
	return (1);
}

void				push_swap_exit(t_ps *elem, int status)
{
	int	i;

	i = 0;
	if (elem->node)
		free(elem->node);
	if (status == EXIT_FAILURE)
		ft_putstr_fd("Error\n", 2);
	exit(status);
}
