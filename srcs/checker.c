/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 17:27:49 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/18 18:00:30 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int			exec_rot_cmd(t_ps *elem, char *cmd)
{
	if (!ft_strcmp("ra", cmd))
		rotate(elem, LIST_A);
	else if (!ft_strcmp("rb", cmd))
		rotate(elem, LIST_B);
	else if (!ft_strcmp("rr", cmd))
	{
		rotate(elem, LIST_A);
		rotate(elem, LIST_B);
	}
	else if (!ft_strcmp("rra", cmd))
		r_rotate(elem, LIST_A);
	else if (!ft_strcmp("rrb", cmd))
		r_rotate(elem, LIST_B);
	else if (!ft_strcmp("rrr", cmd))
	{
		r_rotate(elem, LIST_A);
		r_rotate(elem, LIST_B);
	}
	else
		ft_error();
	return (1);
}

static inline int	exec_cmd(t_ps *elem, char *cmd)
{
	size_t	len;

	if ((len = ft_strlen(cmd)) < 2 || len > 4)
		return (0);
	if (cmd[len - 1] == '\n')
		cmd[--len] = '\0';
	if (!ft_strcmp("sa", cmd))
		swap(elem, LIST_A);
	else if (!ft_strcmp("sb", cmd))
		swap(elem, LIST_B);
	else if (!ft_strcmp("ss", cmd))
	{
		swap(elem, LIST_A);
		swap(elem, LIST_B);
	}
	else if (!ft_strcmp("pa", cmd))
		push(elem, LIST_B);
	else if (!ft_strcmp("pb", cmd))
		push(elem, LIST_A);
	else
		return (exec_rot_cmd(elem, cmd));
	return (1);
}

static void			checker_init(t_ps *elem, int ac, char **av)
{
	ft_bzero(elem, sizeof(t_ps));
	elem->std_in = 0;
	elem->std_out = 1;
	push_swap_init(elem, ac, av);
}

int					main(int ac, char **av)
{
	t_ps	test;
	char	*line;

	if (ac < 2)
		return (0);
	line = NULL;
	checker_init(&test, ac, av);
	if (!ft_strcmp("", av[1]))
		return (0);
	while (get_next_line(0, &line))
	{
		if (!*line || *line == '\n' || exec_cmd(&test, line))
			free(line);
		else if (!ft_strcmp("view a", line))
			view_stack(&test, LIST_A);
		else if (!ft_strcmp("view b", line))
			view_stack(&test, LIST_B);
		else if (!ft_strcmp("stop", line))
			break ;
	}
	!ft_is_sort(&test, LIST_A, FLAG) ? ft_putstr("KO\n") : ft_putstr("OK\n");
	push_swap_exit(&test, EXIT_SUCCESS);
}
