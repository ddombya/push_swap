/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   selectsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 12:23:54 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/09 18:49:52 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void			list_rotate(t_ps *elem, uint8_t stack, uint8_t p_min)
{
	if (p_min == 1)
		swap(elem, stack);
	else if (p_min > 1)
	{
		if (p_min <= (elem->list[stack].len / 2))
		{
			while (p_min--)
				rotate(elem, stack);
		}
		else
		{
			while (elem->list[stack].len - p_min)
			{
				r_rotate(elem, stack);
				++p_min;
			}
		}
	}
}

static void			selectsort_a(t_ps *elem, uint8_t stack)
{
	t_node		*node;
	int			min;
	uint8_t		p_min[2];

	if (!elem->list[stack].len)
		return ;
	if (elem->list[stack].len <= 3)
		return (short_sort(elem, stack, elem->list[stack].len));
	node = stack_head(elem, stack);
	min = INT_MAX;
	ft_bzero(p_min, 2 * sizeof(u_int8_t));
	while (node != (t_node *)(elem->list + stack))
	{
		if (node->nb < min)
		{
			min = node->nb;
			p_min[0] = p_min[1];
		}
		++p_min[1];
		node = node->next;
	}
	list_rotate(elem, stack, p_min[0]);
	push(elem, stack);
	selectsort_a(elem, stack);
	push(elem, LIST_B);
}

void				selectsort(t_ps *elem, uint8_t stack)
{
	t_node		*node;
	int			max;
	uint8_t		p_min[2];

	if (stack == LIST_A)
		return (selectsort_a(elem, stack));
	if (!elem->list[stack].len)
		return ;
	node = stack_head(elem, stack);
	max = 0;
	ft_bzero(p_min, 2 * sizeof(uint8_t));
	while (node != (t_node *)(elem->list + stack))
	{
		if (node->nb > max)
		{
			max = node->nb;
			p_min[0] = p_min[1];
		}
		++p_min[1];
		node = node->next;
	}
	list_rotate(elem, stack, p_min[0]);
	push(elem, stack);
	selectsort(elem, stack);
}
