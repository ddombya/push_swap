/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 17:05:47 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/18 16:47:27 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		main(int ac, char **av)
{
	int		i;
	int		nb;
	t_ps	test;
	t_node	*tmp;

	i = 0;
	ft_memset(&test, 0, sizeof(t_ps));
	test.std_in = 0;
	test.std_out = 1;
	test.opt = 3;
	push_swap_init(&test, ac, av);
	tmp = test.node;
	while (tmp && i < ((int)test.list[LIST_A].len - 1))
	{
		nb = tmp->next->nb;
		if (nb == tmp->nb)
			ft_error();
		tmp = tmp->next;
		i++;
	}
	if (!ft_is_sort(&test, LIST_A, FLAG))
		sort(&test, LIST_A, test.list[LIST_A].len);
	push_swap_exit(&test, EXIT_SUCCESS);
}
