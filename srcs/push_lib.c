/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_lib.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 15:52:00 by ddombya           #+#    #+#             */
/*   Updated: 2018/07/09 18:47:12 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

inline static char	*print_cmd(uint8_t cmd)
{
	static char *str[] = {
		[op_s + LIST_A] = "sa\n",
		[op_s + LIST_B] = "sb\n",
		[op_s + BOTH_LIST] = "ss\n",
		[op_p + LIST_B] = "pa\n",
		[op_p + LIST_A] = "pb\n",
		[op_r + LIST_A] = "ra\n",
		[op_r + LIST_B] = "rb\n",
		[op_r + BOTH_LIST] = "rr\n",
		[op_rr + LIST_A] = "rra\n",
		[op_rr + LIST_B] = "rrb\n",
		[op_rr + BOTH_LIST] = "rrr\n"
	};

	return ((cmd > (op_rr + BOTH_LIST) ? NULL : str[cmd]));
}

inline void			swap(t_ps *elem, uint8_t stack)
{
	t_stack *s;
	char	*str;

	s = elem->list + stack;
	if (!s->len)
		return ;
	ft_swap_node(s->head);
	if (elem->opt == PRINT_OP)
	{
		str = print_cmd(op_s + stack);
		write(elem->std_out, str, ft_strlen(str));
	}
	else if (elem->opt == OPT_V)
		view_stack(elem, BOTH_LIST);
}

inline void			push(t_ps *elem, uint8_t stack)
{
	t_stack	*s1;
	t_stack	*s2;
	t_node	*node;
	char	*str;

	s1 = elem->list + stack;
	s2 = elem->list + (stack ^ 1);
	if (!s1->len)
		return ;
	node = s1->head;
	s1->len--;
	ft_push_elem(node, node->prev, node->next);
	ft_stack_pop(s2, node);
	if (elem->opt == PRINT_OP)
	{
		str = print_cmd(op_p + stack);
		write(elem->std_out, str, ft_strlen(str));
	}
	else if (elem->opt == OPT_V)
		view_stack(elem, BOTH_LIST);
}

inline void			rotate(t_ps *elem, uint8_t stack)
{
	t_stack *s;
	char	*str;

	s = elem->list + stack;
	if (!s->len)
		return ;
	ft_swap_node((t_node *)s);
	if (elem->opt == PRINT_OP)
	{
		str = print_cmd(op_r + stack);
		write(elem->std_out, str, ft_strlen(str));
	}
	else if (elem->opt == OPT_V)
		view_stack(elem, BOTH_LIST);
}

inline void			r_rotate(t_ps *elem, uint8_t stack)
{
	t_stack *s;
	char	*str;

	s = elem->list + stack;
	if (!s->len)
		return ;
	ft_swap_node(s->tail);
	if (elem->opt == PRINT_OP)
	{
		str = print_cmd(op_rr + stack);
		write(elem->std_out, str, ft_strlen(str));
	}
	else if (elem->opt == OPT_V)
		view_stack(elem, BOTH_LIST);
}
