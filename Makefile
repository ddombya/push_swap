# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ddombya <ddombya@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/27 15:58:46 by ddombya           #+#    #+#              #
#    Updated: 2018/07/09 19:38:28 by ddombya          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

NAME2 = checker

CC = gcc

CFLAGS = -Wall -Wextra -Werror

LIB = libft

LDFLAGS = -Llibft

LDLIBS = -lft

SRC_PATH = srcs

SRC_NAME =	list_fcts.c\
			sort.c\
			push_lib.c\
			quicksort.c\
			selectsort.c\
			push_swap.c\
			stack_fcts.c

SRC_NAME2 = list_fcts.c\
			sort.c\
			push_lib.c\
			quicksort.c\
			selectsort.c\
			checker.c\
			stack_fcts.c

AR = ar rc

INC_LIB = -I libft

CPPFLAGS = -I includes

OBJ_PATH = obj

OBJ_PATH2 = obj2

OBJ_NAME = $(SRC_NAME:.c=.o)

OBJ_NAME2 = $(SRC_NAME2:.c=.o)

LOG_CLEAR		= \033[2K
LOG_UP			= \033[A
LOG_NOCOLOR		= \033[0m
LOG_BOLD		= \033[1m
LOG_UNDERLINE	= \033[4m
LOG_BLINKING	= \033[5m
LOG_BLACK		= \033[1;30m
LOG_RED			= \033[1;31m
LOG_GREEN		= \033[1;32m
LOG_YELLOW		= \033[1;33m
LOG_BLUE		= \033[1;34m
LOG_VIOLET		= \033[1;35m
LOG_CYAN		= \033[1;36m
LOG_WHITE		= \033[1;37m

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))
SRC2 = $(addprefix $(SRC_PATH)/,$(SRC_NAME2))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))
OBJ2 = $(addprefix $(OBJ_PATH2)/,$(OBJ_NAME2))

all: $(NAME) $(NAME2)

$(NAME): $(OBJ)
	@make -C $(LIB)
	@$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

$(NAME2): $(OBJ2)
	@make -C $(LIB)
	@$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@echo -e "--$(LOG_CLEAR)$(LOG_VIOLET)$(NAME)$(LOG_NOCOLOR)........................ $(LOG_YELLOW)$<$(LOG_NOCOLOR)$(LOG_UP)"
	@mkdir -p $(OBJ_PATH) 2> /dev/null || true
	@$(CC) $(CFLAGS) $(CPPFLAGS) $(INC_LIB) -o $@ -c $<

$(OBJ_PATH2)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH2) 2> /dev/null || true
	@$(CC) $(CFLAGS) $(CPPFLAGS) $(INC_LIB) -o $@ -c $<

clean:
	@echo -e "$(LOG_CLEAR)$(LOG_BLUE)clean obj$(LOG_NOCOLOR)"
	@make fclean -C $(LIB)
	@rm -f $(OBJ) $(OBJ2)
	@rm -rf $(OBJ_PATH) || true
	@rm -rf $(OBJ_PATH2) || true

fclean: clean
	@echo -e "$(LOG_CLEAR)$(LOG_BLUE)clean exe$(LOG_NOCOLOR)"
	@rm -f $(NAME) $(NAME2)

re: fclean all
